/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import entidad.Mueble;

/**
 *
 * @author ARMAND
 */
public class CarroItem 
{
     Mueble mueble;
    short cantidad;

    public CarroItem(Mueble libro) {
        this.mueble = libro;
        cantidad = 1;
    }

    public Mueble getMueble() {
        return mueble;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short cantidad) {
        this.cantidad = cantidad;
    }

    public void incrementaCantidad() {
        cantidad++;
    }

    public void reduceCantidad() {
        cantidad--;
    }

    public double getTotal() {
        double amount = 0;
        amount = (this.getCantidad() * mueble.getPrecio().doubleValue());
        return amount;
    }
    
}
