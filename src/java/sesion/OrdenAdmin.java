/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import carro.Carro;
import carro.CarroItem;
import entidad.Cliente;
import entidad.Mueble;
import entidad.Orden;
import entidad.Ordenmueble;
import entidad.OrdenmueblePK;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ARMAND
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OrdenAdmin 
{
    @PersistenceContext(unitName = "MuebleriaFPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    @EJB
    private MuebleFacade muebleFacade;
    @EJB
    private OrdenFacade clienteOrdenFacade;
    @EJB
    private OrdenmuebleFacade ordenMuebleFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int placeOrden(String nombre, String email, String telefono, String direccion, String cp, String tarjNumero, Carro carro) {

        try {
            Cliente cliente = agregaCliente(nombre, email, telefono, direccion, cp, tarjNumero);
            Orden orden = agregaOrden(cliente, carro);
            agregaItemsAOrden(orden, carro);
            return orden.getIdOrden();
        } catch (Exception e) {
            e.printStackTrace();
            context.setRollbackOnly();
            return 0;
        }
    }

    private Cliente agregaCliente(String nombre, String email, String telefono, String direccion, String cp, String tarjNumero) {
        Cliente cliente = new Cliente();

        System.out.println(nombre);
        System.out.println(email);
        System.out.println(direccion);
        System.out.println(cp);
        System.out.println(tarjNumero);

        cliente.setNombre(nombre);
        cliente.setEmail(email);
        cliente.setTelefono(telefono);
        cliente.setDireccion(direccion);
        cliente.setCp(cp);
        cliente.setTarjNum(tarjNumero);;

        em.persist(cliente);
        return cliente;
    }

    private Orden agregaOrden(Cliente cliente, Carro carro) {

        System.out.println("elerror en agregaOrden");
        Orden orden = new Orden();
        orden.setIdCliente(cliente);
        orden.setTotal(BigDecimal.valueOf(carro.getTotal()));

        java.util.Date dt = new java.util.Date();


        Random random = new Random();
        int i = random.nextInt(999999999);
        orden.setFechaCreacion(dt);
        orden.setNumConfirmacio(i);

        em.persist(orden);
        return orden;
    }

    private void agregaItemsAOrden(Orden orden, Carro carro) {
        System.out.println("elerror en agregaItemsAOrden");
        em.flush();

        List<CarroItem> items = carro.getItems();

        for (CarroItem scItem : items) {

            int muebleId = scItem.getMueble().getIdMueble();

            OrdenmueblePK ordenedMueblePK = new OrdenmueblePK();
            ordenedMueblePK.setIdOrden(orden.getIdOrden());
            ordenedMueblePK.setIdMueble(muebleId);

 
            Ordenmueble ordenItem = new Ordenmueble(ordenedMueblePK);
            ordenItem.setCantidad(scItem.getCantidad());

            em.persist(ordenItem);
        }
    }

    public Map getOrdenDetalles(int ordenId) {

        Map ordenMap = new HashMap();

        Orden orden = clienteOrdenFacade.find(ordenId);

        Cliente cliente = orden.getIdCliente();

        List<Ordenmueble> Muebles = ordenMuebleFacade.findByOrdenId(ordenId);

        List<Mueble> muebles = new ArrayList<>();

        for (Ordenmueble op : Muebles) 
        {

            Mueble p = (Mueble) muebleFacade.find(op.getOrdenmueblePK().getIdMueble());
            muebles.add(p);
        }

        ordenMap.put("regOrden", orden);
        ordenMap.put("cliente", cliente);
        ordenMap.put("ordenedMuebles", Muebles);
        ordenMap.put("muebles", muebles);

        return ordenMap;
    }

}
