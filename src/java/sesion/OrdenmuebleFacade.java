/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Ordenmueble;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ARMAND
 */
@Stateless
public class OrdenmuebleFacade extends AbstractFacade<Ordenmueble> {
    @PersistenceContext(unitName = "MuebleriaFPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdenmuebleFacade() {
        super(Ordenmueble.class);
    }
    public List<Ordenmueble> findByOrdenId(Object ordenId) 
    {
    return em.createNamedQuery("Ordenmueble.findByIdOrden").setParameter("idOrden", ordenId).getResultList();    
    }
    
}
