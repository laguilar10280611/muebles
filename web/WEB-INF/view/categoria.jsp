<%-- 
    Document   : categoria
    Created on : 2/12/2014, 11:45:53 PM
    Author     : ARMAND
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <div style="background:  azure">
    <br/><br/>

    <table align="center">

        <c:forEach var="categoria" items="${categorias}">
            <td width="200px" align="center" >
                <a href="<c:url value='categoria?${categoria.idCategoria}'/>">
                    <span  ></span>
                    <span  >${categoria.nombre}</span>
                    <br/>

                    <img    width="100" src="${initParam.categoriasImg}${categoria.nombreImg}.gif"
                            alt="${categoria.nombreImg}"  >
                </a>
                <br/>
            </td>
        </c:forEach>

        <tr><td>
                <div align="center">
                    <br/><br/>    
                    <c:forEach var="categoria" items="${categorias}">

                        <c:choose>
                            <c:when test="${categoria.nombre == categoSel.nombre}">
                                <div   id="categoSel">
                                    <span  >
                                        -${categoria.nombre}
                                    </span>
                                </div>
                            </c:when>

                        </c:choose>

                    </c:forEach>
                    <br/><br/>
                </div>
            </td></tr>



        <c:forEach var="mueble" items="${categoriaMuebles}" varStatus="iter">

            <tr>

                <td>
                    <img src="${initParam.mueblesImg}${mueble.nombreImg}.jpg"
                         alt="${mueble.nombreImg}">
                </td>

                <td>
                    ${mueble.nombre}
                    <br>
                    <span  >${mueble.descipcion}</span>
                    <br>
                    <span  >Autor: ${mueble.marca}</span>
                </td>

                <td>$ ${mueble.precio}</td>

                <td>
                    <form action="<c:url value='agregaACarro'/>" method="post">
                        <input type="hidden"
                               name="muebleId"
                               value="${mueble.idMueble}">
                        <input type="submit"
                               name="submit"
                               value="añadir a carrito">
                    </form>
                </td>
            </tr>

        </c:forEach>


    </table>

</div>
        
    </body>
</html>
