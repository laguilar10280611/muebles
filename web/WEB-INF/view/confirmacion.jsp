<%-- 
    Document   : confirmacion
    Created on : 2/12/2014, 11:46:02 PM
    Author     : ARMAND
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <div style="background:  azure">


    <p >
        <strong>Su orden esta lista para ser entregada en una semana</strong>
        <br><br>
        conserve el numero de confirmacion de orden:
        <strong>${regOrden.numConfirmacion}</strong>
        <br>
    </p>


    <table class="tablex"   >
        <tr  >
            <th colspan="4">Contenido de la orden</th>
        </tr>

        <tr  >
            <td>Titulo</td>
            <td>Precio unitario</td>
            <td>Cantidad</td>
            <td>Precio</td>
        </tr>

        <c:forEach var="ordenedMueble" items="${ordenedMuebles}" varStatus="iter">

            <tr>
                <td>${muebles[iter.index].nombre}</td>
                <td  >
                    $ ${muebles[iter.index].precio}
                </td>
                <td  >
                    ${ordenedMueble.cantidad}
                </td>
                <td  >
                    $ ${muebles[iter.index].precio * ordenedMueble.cantidad}
                </td>
            </tr>

        </c:forEach>

        <tr  ><td colspan="4" style="padding: 0 20px"><hr></td></tr>

        <tr  >
            <td colspan="3" id="cargoDeEnvioCellLeft"><strong>gastos de envio:</strong></td>
            <td >$ ${initParam.cargoDeEnvio}</td>
        </tr>

        <tr  >
            <td  colspan="3"  id="totalCellLeft"><strong>total:</strong></td>
            <td >$ ${regOrden.total}</td>
        </tr>

        <tr  ><td colspan="4" style="padding: 0 20px"><hr></td></tr>

        <tr  >
            <td colspan="4" ><strong>fecha del proceso:</strong>
                ${regOrden.fechaCreacion}
            </td>
        </tr>
    </table>

    <table class="tablex"   >
        <tr  >
            <th colspan="4">datos del pedido</th>
        </tr>

        <tr>
            <td colspan="4"  >
                <b>Nombre: </b> ${cliente.nombre}
                <br>
                <b>Direccion: </b> ${cliente.direccion}
                <br>
                <b>C.P: </b> ${cliente.cp}
                <br>
                <hr>
                <strong>email:</strong> ${cliente.email}
                <br>
                <strong>telefono:</strong> ${cliente.telefono}
            </td>
        </tr>
    </table>

</div>
        
    </body>
</html>
