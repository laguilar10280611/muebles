<%-- 
    Document   : pago
    Created on : 2/12/2014, 11:46:10 PM
    Author     : ARMAND
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <div style="background:  azure">
    <br/>
    <div align="center">

        <h2 >Realizar Pago</h2>

        <p>Para concluir tu compra, proporcionar la siguiente imformacion:</p>
        <script src="js/jquery-1.4.2.js" type="text/javascript"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>        
        <script type="text/javascript">
    $().ready(function(){
	$("#pago").validate({
            rules:{
                name:{ required:true, minlength:5},
                email:{required:true, email:true},
                cp:{required:true, number:true, maxlength:5, minlength:5 },
                telefono:{required:true, number:true, maxlength:12, minlength:10 },
                direccion:{required:true,minlength:45},                
                creditcard:{required:true, creditcard:true}
            },
            messages:{
                name:{ required:"Campo obligatorio",minlength:"ingrese minimo 5 caracteres" },
                email:{required:"Campo obligatorio", email:"Introdusca un email valido"},
                telefono:{required:"Campo obligatorio", number:"Solo numeros", maxlength:"Maximo 12 numeros", minlength:"Minimo 10 numeros"},
                direccion:{required:"Campo obligatorio", minlength:"Minimo 45 caracteres"},
                cp:{required:"Campo obligatorio",  number:"Solo numeros",maxlength:"Maximo 5 numeros", minlength:"minimo 5 numeros" },
                creditcard:{required:"Campo obligatorio", creditcard:"Introdusca una tarjeta de credito valido"}
           }
            
	});
    });
        </script>        
        <form  action="<c:url value='hacerpago'/>" method="post" id="pago" name="pago" >
            <table >
                <tr>
                    <td><label for="name">Nombre</label></td>
                    <td  >
                        <input type="text"
                               size="31"
                               maxlength="45"
                               id="name"
                               name="name"
                               value="${param.name}">
                    </td>
                </tr>
                <tr>
                    <td><label for="email">Correo</label></td>
                    <td  >
                        <input type="text"
                               size="31"
                               maxlength="45"
                               id="email"
                               name="email"
                               class="required"
                               value="${param.email}">
                    </td>
                </tr>
                <tr>
                    <td><label for="telefono">Telefono</label></td>
                    <td  >
                        <input type="text"
                               size="31"
                               maxlength="16"
                               id="telefono"
                               name="telefono"
                               value="${param.telefono}">
                    </td>
                </tr>
                <tr>
                    <td><label for="direccion">Direccion</label></td>
                    <td  >
                        <input type="text"
                               size="31"
                               maxlength="45"
                               id="direccion"
                               name="direccion"
                               value="${param.direccion}">

                    </td>
                </tr>
                
                
                
                                
                    <td><label for="Codigo Postal">Codigo Postal</label></td>
                    <td  >
                        <input type="text"
                               size="31"
                               maxlength="45"
                               id="zipcode"
                               name="cp"
                               value="${param.cp}">

                    </td>
                
                
                <tr>
                    <td><label for="creditcard">Numero de Tarjeta de Credito</label></td>
                    <td  >
                        <input type="text"
                               size="31"
                               maxlength="19"
                               id="creditcard"
                               name="creditcard"
                               value="${param.creditcard}">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Concluir compra">
                    </td>
                </tr>
            </table>
        </form>
        <ul>
            <li>Cargos de envio</li>
            <li>Se cobrara  ${initParam.cargoDeEnvio}
                por envio a cualquier parte de la republica Mexicana</li>
        </ul>

        <table >
            <tr>
                <td>subtotal:</td>
                <td  >
                    $ ${carro.subtotal}</td>
            </tr>
            <tr>
                <td>Cargo de Envio</td>
                <td  >
                    $ ${initParam.cargoDeEnvio}</td>
            </tr>
            <tr>
                <td  >Total:</td>
                <td  >
                    $ ${carro.total}</td>
            </tr>
        </table>

    </div>
</div>
        
        
        
    </body>
</html>
