<%-- 
    Document   : index
    Created on : 2/12/2014, 11:20:59 PM
    Author     : ARMAND
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <LINK REL=StyleSheet href="css/estilo.css" TYPE="text/css">
    </head>
    <body>
            <table align="center" >
                <tr>
                    <td width="374px">  <p style="font-size: larger">Bienvenido a El mueble Chiflado!, Contamos con los mejores muebles, la mejor calidad, y los muebles mas vendidos de 
                            del mercado</p>
                        <p>Ruta para las imágenes de Categorías: ${initParam.categoriasImg}</p>
        <p>Ruta para las imágenes de Muebles ${initParam.mueblesImg}</p>
       
                    </td>
                </tr>
                <tr>
               
                    
                    
                        <c:forEach var="categoria" items="${categorias}">
                            <td>
                                <a href="<c:url value='categoria?${categoria.idCategoria}'/>">
                                    <span></span>
                                    <span  >${categoria.nombre}</span>
                                    <br/>
                                    <div class="imagencatego">

                                    <img  src="${initParam.categoriasImg}${categoria.nombreImg}.jpg"
                                          alt="${categoria.nombreImg}">
                                    </div> 
                                </a>
                            </td>
                        </c:forEach>
                
                </tr> 

        </table>
        
        
        
    </body>
</html>
